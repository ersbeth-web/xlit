# xLit

Additional utilities for [LitElement](https://lit.dev/).

## Installation
```  npm install @ersbeth/xlit```

## Usage

This package provides utilities organized in the following categories:
* elements
* directives
* decorators
* animations

You may import them as needed.