import { noChange } from "lit";
import { insertPart, removePart, setChildPartValue, setCommittedValue } from "lit/directive-helpers.js";
import { Directive, type ChildPart, directive } from "lit/directive.js";

/**
 * Keyed directive which keeps old part until its out transition is finished
 */
class KeyedInOut extends Directive {
    key: any;
    currentPart: ChildPart | undefined;

    update(containerPart: ChildPart, [key, func]: unknown[]): unknown {

        if (this.key !== key) { // remove old part and create new one
            this.key = key;
            if (this.currentPart) removePart(this.currentPart);
            this.currentPart = insertPart(containerPart);
            setChildPartValue(this.currentPart, this.render(key, func as Function));
            setCommittedValue(containerPart, this.currentPart);
        }

        else { // update old part
            setChildPartValue(this.currentPart!, this.render(key, func as Function));
        }

        return noChange;
    }

    render(key: any, func: Function) {
        return func();
    }
}

export const keyedInOut = directive(KeyedInOut);


