const flyLeft = [{ transform: 'translateX(-100%)' }]
const flyRight = [{ transform: 'translateX(100%)' }]

/**
 * Pop transition for '@lit-lab/motion'
 */
export const pop = {
    in: flyLeft,
    out: flyRight
};

/**
 * Push transition for '@lit-lab/motion'
 */
export const push = {
    in: flyRight,
    out: flyLeft
};