import { LitElement } from "lit"

/**
 * Custom element useful for unit tests
 */
export class TestElement extends LitElement {
    protected createRenderRoot() { return this }
    protected logEvent(e: CustomEvent) { console.log(e.type, e.detail) }
}
