import type { LitElement, ReactiveController, ReactiveControllerHost } from "lit";

/**
 * Media query reactive controller
 */
export function mediaQuery(query: string) {
    return function (_: any, context: { name: string }) {
        return function initializer(this: LitElement, _: any) {
            const mediaQueryController = new MediaQueryController(this, context.name, query)
            this.addController(mediaQueryController);
            //@ts-ignore
            return this[name];
        }
    }
}

class MediaQueryController implements ReactiveController {

    protected host: ReactiveControllerHost;
    protected propName: string;
    protected mediaQueryList: MediaQueryList;

    constructor(host: ReactiveControllerHost, propName: string, mediaQuery: string) {
        this.host = host;
        this.propName = propName
        this.mediaQueryList = matchMedia(mediaQuery)
        this.updateValue();
    }

    protected updateValue = () => {
        //@ts-ignore
        this.host[this.propName] = this.mediaQueryList.matches;
        this.host.requestUpdate();
    }

    hostConnected(): void {
        this.updateValue();
        this.mediaQueryList.addEventListener('change', this.updateValue);
    }

    hostDisconnected(): void {
        this.mediaQueryList.removeEventListener('change', this.updateValue)
    }
}