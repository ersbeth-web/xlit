import type { LitElement, ReactiveController } from "lit";

export type Callback = (value?: any) => void;

export interface Reaction {
    to: () => undefined | ((callback: Callback) => Function),
    do: Callback,
}

/**
 * Subscribes/Unsubscribes to a signal according to element life cycle
 */
export function react(_: any, context: ClassFieldDecoratorContext) {

    return function initializer(this: LitElement, initialValue: Reaction): any {
        const reactionController = new ReactionController(initialValue);
        this.addController(reactionController);
        return initialValue;
    }
}

class ReactionController implements ReactiveController {

    protected reaction: Reaction;
    protected unsubscribe?: Function;

    constructor(reaction: Reaction) {
        this.reaction = reaction
    }

    hostConnected(): void {
        const signal = this.reaction.to();
        if (signal) {
            this.unsubscribe = signal(this.reaction.do);
        }
        else {
            this.unsubscribe = undefined;
        }
    }

    hostDisconnected(): void {
        if (this.unsubscribe) this.unsubscribe()
    }
}