import { LitElement, type ReactiveController, type ReactiveControllerHost } from "lit";

/**
 * Caching decorator for getters
 */
export function computed(getter: Function, context: ClassGetterDecoratorContext) {
    if (context.kind != "getter") throw `Computed: decorator must be applied to a getter.`

    context.addInitializer(function () {
        if (!(this instanceof LitElement)) throw `Computed: decorator must be used inside a LitElement class`
        const computedController = new ComputedController(this, getter);
        this.addController(computedController);
        //@ts-ignore
        this[`__computed_${context.name}`] = computedController // store controller on the instance
    })

    return function (this: LitElement) {
        //@ts-ignore
        return this[`__computed_${context.name}`].value
    }
}

class ComputedController implements ReactiveController {

    value: any;
    protected host: ReactiveControllerHost;
    protected getter: Function;

    constructor(host: ReactiveControllerHost, getter: Function) {
        this.host = host;
        this.getter = getter
    }

    hostUpdate(): void {
        const value = this.getter.call(this.host)
        // if undefined we keep old value 
        // useful for out-animation reconnection when values are no more available
        if (value != undefined) this.value = value;
    }
}