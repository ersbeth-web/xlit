import { LitElement, type ReactiveController, type ReactiveControllerHost } from "lit";

export interface Observable<T> {
    subscribe: (callback: (value: T) => void) => Function;
    value: any;
}

export interface Signal {
    on: (callback: () => void) => Function
}

export type Subscribable = Observable<any> | Signal

export type ObservableGetter = () => Subscribable | undefined

/**
 * Subscribes/Unsubscribes to an observer according to element life cycle
 */
export function observe(getter: ObservableGetter, context: ClassGetterDecoratorContext): () => any {
    if (context.kind != "getter") throw `Computed: decorator must be applied to a getter.`

    let observerController: ObserverController;

    context.addInitializer(function (this: unknown) {
        if (!(this instanceof LitElement)) throw `Computed: decorator must be used inside a LitElement class`

        observerController = new ObserverController(this, getter)
        this.addController(observerController);
    })

    return () => observerController?.subscribable;
}

class ObserverController implements ReactiveController {

    protected host: ReactiveControllerHost;
    protected getter: ObservableGetter;
    protected unsubscribe?: Function;
    subscribable?: Subscribable

    constructor(host: ReactiveControllerHost, getter: ObservableGetter) {
        this.host = host;
        this.getter = getter
    }

    hostConnected(): void {
        this.subscribable = this.subscribable ?? this.getter.call(this.host)

        if (this.subscribable && "subscribe" in this.subscribable) {
            this.unsubscribe = this.subscribable.subscribe(() => this.host.requestUpdate());
        }
        else if (this.subscribable && "on" in this.subscribable) {
            this.unsubscribe = this.subscribable.on(() => this.host.requestUpdate());
        }
        else {
            this.unsubscribe = undefined;
        }
    }

    hostDisconnected(): void {
        if (this.unsubscribe) this.unsubscribe()
    }
}