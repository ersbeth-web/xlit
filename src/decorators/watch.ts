import type { LitElement, ReactiveController } from "lit";

/**
 * Performs an action at each update.
 */
export function watch(_: any, context: { addInitializer: Function }) {

    context.addInitializer(

        function (this: LitElement) {
            //@ts-ignore
            this[context.name] = this[context.name].bind(this);
            //@ts-ignore
            const watcher = new WatchController(this[context.name]);
            this.addController(watcher);
        }

    );
}

class WatchController implements ReactiveController {
    private action: Function;

    constructor(action: Function) { this.action = action; }

    hostUpdated() { this.action(); }
}