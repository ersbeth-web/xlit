export type EventEmitter<T> = (value: T) => void

/**
 * Shortcut for event handler
 */
export function event(name?: string) {
    return function (_: any, context: any) {

        return function initializer(_: any) {

            return (value: any) => {
                const event = new CustomEvent(name ?? context.name, {
                    detail: value,
                    bubbles: true,
                    composed: true
                });
                //@ts-ignore
                this.dispatchEvent(event); // `this` is bound to class instance here
            }
        }
    }
}