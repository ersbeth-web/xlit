import { property, queryAssignedElements } from "lit/decorators.js";

export const prop = property({ attribute: false })

export const slots = queryAssignedElements;

// export const element = query

