import { LitElement, type ReactiveController, type ReactiveControllerHost } from "lit";

export function element(selector: string) {
    return function decorator(_: any, context: ClassFieldDecoratorContext) {

        if (context.kind !== "field") throw 'xlit - @element: decorator should be applied to a class field'

        return function initalizer(this: LitElement, initialValue: any) {

            if (!(this instanceof LitElement)) throw 'xlit = @element: decorator should be used in a LitElement subclass.'

            const elementController = new ElementController(this, context.name as string, selector);
            this.addController(elementController);
            return initialValue;
        };
    }
}

class ElementController implements ReactiveController {

    protected host: LitElement & ReactiveControllerHost;
    protected selector: string;
    protected fieldName: string;

    constructor(host: LitElement & ReactiveControllerHost, fieldName: string, selector: string) {
        this.host = host;
        this.selector = selector
        this.fieldName = fieldName
    }

    hostUpdated(): void {
        //@ts-ignore
        this.host[this.fieldName] = this.host.shadowRoot!.querySelector(this.selector);
    }
}