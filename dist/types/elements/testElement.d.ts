import { LitElement } from "lit";
/**
 * Custom element useful for unit tests
 */
export declare class TestElement extends LitElement {
    protected createRenderRoot(): this;
    protected logEvent(e: CustomEvent): void;
}
//# sourceMappingURL=testElement.d.ts.map