import { Directive, type ChildPart } from "lit/directive.js";
/**
 * Keyed directive which keeps old part until its out transition is finished
 */
declare class KeyedInOut extends Directive {
    key: any;
    currentPart: ChildPart | undefined;
    update(containerPart: ChildPart, [key, func]: unknown[]): unknown;
    render(key: any, func: Function): any;
}
export declare const keyedInOut: (key: any, func: Function) => import("lit-html/directive.js").DirectiveResult<typeof KeyedInOut>;
export {};
//# sourceMappingURL=keyedInOut.d.ts.map