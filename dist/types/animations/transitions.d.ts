/**
 * Pop transition for '@lit-lab/motion'
 */
export declare const pop: {
    in: {
        transform: string;
    }[];
    out: {
        transform: string;
    }[];
};
/**
 * Push transition for '@lit-lab/motion'
 */
export declare const push: {
    in: {
        transform: string;
    }[];
    out: {
        transform: string;
    }[];
};
//# sourceMappingURL=transitions.d.ts.map