import type { LitElement } from "lit";
/**
 * Media query reactive controller
 */
export declare function mediaQuery(query: string): (_: any, context: {
    name: string;
}) => (this: LitElement, _: any) => any;
//# sourceMappingURL=mediaQuery.d.ts.map