import { LitElement } from "lit";
export declare function element(selector: string): (_: any, context: ClassFieldDecoratorContext) => (this: LitElement, initialValue: any) => any;
//# sourceMappingURL=element.d.ts.map