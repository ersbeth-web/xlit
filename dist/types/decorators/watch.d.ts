/**
 * Performs an action at each update.
 */
export declare function watch(_: any, context: {
    addInitializer: Function;
}): void;
//# sourceMappingURL=watch.d.ts.map