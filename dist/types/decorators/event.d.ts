export type EventEmitter<T> = (value: T) => void;
/**
 * Shortcut for event handler
 */
export declare function event(name?: string): (_: any, context: any) => (_: any) => (value: any) => void;
//# sourceMappingURL=event.d.ts.map