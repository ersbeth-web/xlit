import type { LitElement } from "lit";
export type Callback = (value?: any) => void;
export interface Reaction {
    to: () => undefined | ((callback: Callback) => Function);
    do: Callback;
}
/**
 * Subscribes/Unsubscribes to a signal according to element life cycle
 */
export declare function react(_: any, context: ClassFieldDecoratorContext): (this: LitElement, initialValue: Reaction) => any;
//# sourceMappingURL=react.d.ts.map