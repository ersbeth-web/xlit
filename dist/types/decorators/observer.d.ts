export interface Observable<T> {
    subscribe: (callback: (value: T) => void) => Function;
    value: any;
}
export interface Signal {
    on: (callback: () => void) => Function;
}
export type Subscribable = Observable<any> | Signal;
export type ObservableGetter = () => Subscribable | undefined;
/**
 * Subscribes/Unsubscribes to an observer according to element life cycle
 */
export declare function observe(getter: ObservableGetter, context: ClassGetterDecoratorContext): () => any;
//# sourceMappingURL=observer.d.ts.map