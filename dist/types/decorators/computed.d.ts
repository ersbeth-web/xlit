import { LitElement } from "lit";
/**
 * Caching decorator for getters
 */
export declare function computed(getter: Function, context: ClassGetterDecoratorContext): (this: LitElement) => any;
//# sourceMappingURL=computed.d.ts.map