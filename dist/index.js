// src/decorators/watch.ts
function watch(_, context) {
  context.addInitializer(
    function() {
      this[context.name] = this[context.name].bind(this);
      const watcher = new WatchController(this[context.name]);
      this.addController(watcher);
    }
  );
}
var WatchController = class {
  action;
  constructor(action) {
    this.action = action;
  }
  hostUpdated() {
    this.action();
  }
};

// src/decorators/mediaQuery.ts
function mediaQuery(query) {
  return function(_, context) {
    return function initializer(_2) {
      const mediaQueryController = new MediaQueryController(this, context.name, query);
      this.addController(mediaQueryController);
      return this[name];
    };
  };
}
var MediaQueryController = class {
  host;
  propName;
  mediaQueryList;
  constructor(host, propName, mediaQuery2) {
    this.host = host;
    this.propName = propName;
    this.mediaQueryList = matchMedia(mediaQuery2);
    this.updateValue();
  }
  updateValue = () => {
    this.host[this.propName] = this.mediaQueryList.matches;
    this.host.requestUpdate();
  };
  hostConnected() {
    this.updateValue();
    this.mediaQueryList.addEventListener("change", this.updateValue);
  }
  hostDisconnected() {
    this.mediaQueryList.removeEventListener("change", this.updateValue);
  }
};

// src/decorators/event.ts
function event(name2) {
  return function(_, context) {
    return function initializer(_2) {
      return (value) => {
        const event2 = new CustomEvent(name2 ?? context.name, {
          detail: value,
          bubbles: true,
          composed: true
        });
        this.dispatchEvent(event2);
      };
    };
  };
}

// src/decorators/aliases.ts
import { property, queryAssignedElements } from "lit/decorators.js";
var prop = property({ attribute: false });
var slots = queryAssignedElements;

// src/decorators/computed.ts
import { LitElement } from "lit";
function computed(getter, context) {
  if (context.kind != "getter")
    throw `Computed: decorator must be applied to a getter.`;
  context.addInitializer(function() {
    if (!(this instanceof LitElement))
      throw `Computed: decorator must be used inside a LitElement class`;
    const computedController = new ComputedController(this, getter);
    this.addController(computedController);
    this[`__computed_${context.name}`] = computedController;
  });
  return function() {
    return this[`__computed_${context.name}`].value;
  };
}
var ComputedController = class {
  value;
  host;
  getter;
  constructor(host, getter) {
    this.host = host;
    this.getter = getter;
  }
  hostUpdate() {
    const value = this.getter.call(this.host);
    if (value != void 0)
      this.value = value;
  }
};

// src/decorators/element.ts
import { LitElement as LitElement2 } from "lit";
function element(selector) {
  return function decorator(_, context) {
    if (context.kind !== "field")
      throw "xlit - @element: decorator should be applied to a class field";
    return function initalizer(initialValue) {
      if (!(this instanceof LitElement2))
        throw "xlit = @element: decorator should be used in a LitElement subclass.";
      const elementController = new ElementController(this, context.name, selector);
      this.addController(elementController);
      return initialValue;
    };
  };
}
var ElementController = class {
  host;
  selector;
  fieldName;
  constructor(host, fieldName, selector) {
    this.host = host;
    this.selector = selector;
    this.fieldName = fieldName;
  }
  hostUpdated() {
    this.host[this.fieldName] = this.host.shadowRoot.querySelector(this.selector);
  }
};

// src/decorators/observer.ts
import { LitElement as LitElement3 } from "lit";
function observe(getter, context) {
  if (context.kind != "getter")
    throw `Computed: decorator must be applied to a getter.`;
  let observerController;
  context.addInitializer(function() {
    if (!(this instanceof LitElement3))
      throw `Computed: decorator must be used inside a LitElement class`;
    observerController = new ObserverController(this, getter);
    this.addController(observerController);
  });
  return () => observerController?.subscribable;
}
var ObserverController = class {
  host;
  getter;
  unsubscribe;
  subscribable;
  constructor(host, getter) {
    this.host = host;
    this.getter = getter;
  }
  hostConnected() {
    this.subscribable = this.subscribable ?? this.getter.call(this.host);
    if (this.subscribable && "subscribe" in this.subscribable) {
      this.unsubscribe = this.subscribable.subscribe(() => this.host.requestUpdate());
    } else if (this.subscribable && "on" in this.subscribable) {
      this.unsubscribe = this.subscribable.on(() => this.host.requestUpdate());
    } else {
      this.unsubscribe = void 0;
    }
  }
  hostDisconnected() {
    if (this.unsubscribe)
      this.unsubscribe();
  }
};

// src/decorators/react.ts
function react(_, context) {
  return function initializer(initialValue) {
    const reactionController = new ReactionController(initialValue);
    this.addController(reactionController);
    return initialValue;
  };
}
var ReactionController = class {
  reaction;
  unsubscribe;
  constructor(reaction) {
    this.reaction = reaction;
  }
  hostConnected() {
    const signal = this.reaction.to();
    if (signal) {
      this.unsubscribe = signal(this.reaction.do);
    } else {
      this.unsubscribe = void 0;
    }
  }
  hostDisconnected() {
    if (this.unsubscribe)
      this.unsubscribe();
  }
};

// src/directives/keyedInOut.ts
import { noChange } from "lit";
import { insertPart, removePart, setChildPartValue, setCommittedValue } from "lit/directive-helpers.js";
import { Directive, directive } from "lit/directive.js";
var KeyedInOut = class extends Directive {
  key;
  currentPart;
  update(containerPart, [key, func]) {
    if (this.key !== key) {
      this.key = key;
      if (this.currentPart)
        removePart(this.currentPart);
      this.currentPart = insertPart(containerPart);
      setChildPartValue(this.currentPart, this.render(key, func));
      setCommittedValue(containerPart, this.currentPart);
    } else {
      setChildPartValue(this.currentPart, this.render(key, func));
    }
    return noChange;
  }
  render(key, func) {
    return func();
  }
};
var keyedInOut = directive(KeyedInOut);

// src/elements/testElement.ts
import { LitElement as LitElement4 } from "lit";
var TestElement = class extends LitElement4 {
  createRenderRoot() {
    return this;
  }
  logEvent(e) {
    console.log(e.type, e.detail);
  }
};

// src/animations/transitions.ts
var flyLeft = [{ transform: "translateX(-100%)" }];
var flyRight = [{ transform: "translateX(100%)" }];
var pop = {
  in: flyLeft,
  out: flyRight
};
var push = {
  in: flyRight,
  out: flyLeft
};
export {
  TestElement,
  computed,
  element,
  event,
  keyedInOut,
  mediaQuery,
  observe,
  pop,
  prop,
  push,
  react,
  slots,
  watch
};
//# sourceMappingURL=index.js.map
